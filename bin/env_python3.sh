#!/bin/bash 

# Project root directory
PROJECTROOT="$(readlink -f -- "$(dirname "$(readlink -f -- "${BASH_SOURCE[0]}")")/../")"
export PROJECTROOT

# Activate conda
CONDA_BASE=$(conda info --base)
source $CONDA_BASE/etc/profile.d/conda.sh
conda activate "$PROJECTROOT/env"

# Add custom module path
PYTHONPATH="$PYTHONPATH:$PROJECTROOT/module"
export PYTHONPATH

python3 "$@" 1>&1 2>&2

