#!/bin/bash

PROJECTROOT="$(readlink -f -- "$(dirname "$(readlink -f -- "${BASH_SOURCE[0]}")")/../")"

conda env update -f "$PROJECTROOT/config/conda.yml" --prefix "$PROJECTROOT/env" --prune
