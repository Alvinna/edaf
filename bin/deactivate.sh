#!/bin/bash

# Project root directory
unset PROJECTROOT

# Deactivate conda
conda deactivate
