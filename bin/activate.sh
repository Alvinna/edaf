#!/bin/bash

# Project root directory
PROJECTROOT="$(readlink -f -- "$(dirname "$(readlink -f -- "${BASH_SOURCE[0]}")")/../")"
export PROJECTROOT

# Activate conda
conda activate $PROJECTROOT/env

# Add custom module path
PYTHONPATH="$PYTHONPATH:$PROJECTROOT/module"
export PYTHONPATH