#!/bin/bash -i

# Project root directory
PROJECTROOT="$(readlink -f -- "$(dirname "$(readlink -f -- "${BASH_SOURCE[0]}")")/../")"
export PROJECTROOT

# Activate conda
conda activate $PROJECTROOT/env

# Add custom module path
PYTHONPATH="$PYTHONPATH:$PROJECTROOT/module"
export PYTHONPATH

host="$1"
nprocs="$2"
njobs="$3"

if [ "${njobs}" = "1" ]
then
    qsub -N elves -b y -l "walltime=960:00:00" -l "nodes=1:ppn=${nprocs}" -F "${PROJECTROOT} ${host}:8786" ${PROJECTROOT}/tools/env_dask-worker
else
    qsub -N elves -b y -l "walltime=960:00:00" -l "nodes=1:ppn=${nprocs}" -t 1-${njobs} -F "${PROJECTROOT} ${host}:8786" ${PROJECTROOT}/tools/env_dask-worker
fi