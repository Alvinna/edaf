

import tempfile
import shutil
import os
import pymol as pm
import numpy as np
from pymol.cgo import *
import pandas as pd

def dotsplot( filename, objname ):
    """
    @param filename: Input filename
    @param objname: Object name
    """
    dots = pd.read_csv(filename)
    obj = [ BEGIN, POINTS ]
    for i, r in dots.iterrows():
        obj.append( COLOR )
        obj.append( r["r"] )
        obj.append( r["g"] )
        obj.append( r["b"] )
        obj.append( VERTEX )
        obj.append( r["x"] )
        obj.append( r["y"] )
        obj.append( r["z"] )
    obj.append( END )
    pm.cmd.load_cgo( obj, objname )
    return

cmd.extend("dotsplot", dotsplot)
