from PLI.__init__ import __version__


def intro():
    info = """
                                        ===== Interaction Table {0} =====
            This program is written by Jing-wen, Chen from National Chiao Tung University in Hsinchu, Taiwan. If you 
        have any question, comments, or suggestion, please don't be shy to contact me. 
                                        Contact email: roy94621@gmail.com

                                            ===== Introduction =====
            This program is a python-implemented algorithm for analyzing ligand-protein binding. It can identify some
        key interaction between ligand and protein like hydrogen bond, hydrophobic, and pi-pi interaction.

                                                [End of intro]\n""".format(__version__)

    return info
