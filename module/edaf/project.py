import os
import edaf.config as ec

class project:
    def __init__(self):

        # Read project root from environment variable
        pr = os.environ["PROJECTROOT"]
        self.root = pr 

        # Read project configuration
        self.config = ec.read(f"{pr}/project.toml")
        self.name = self.config["name"]

        # Read hosts configuration
        self.hosts = ec.read(f"{pr}/hosts.toml")

    def getHostAddress(self, hostname):
        t = self.hosts.get(hostname).get("host")
        if t:
            return t
        else:
            return "localhost"
    
    def getHostType(self, hostname):
        t = self.hosts.get(hostname).get("type")
        if t:
            return t
        elif self.getHostAddress(hostname) == "localhost":
            return "local"
        else:
            return None

    def getEDAFRoot(self, hostname):
        t = self.hosts.get(hostname).get("type")
        if t == "local":
            return self.root
        else:
            return self.hosts.get(hostname).get("root")
