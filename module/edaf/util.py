import subprocess
import socket
import time
import paramiko
import edaf.project as ep

def download_scp(rhost, rpath, lpath):
    return subprocess.run(["scp", "-rq", f"{rhost}:{rpath}", lpath])

def upload_scp(lpath, rhost, rpath):
    return subprocess.run(["scp", "-rq", lpath, f"{rhost}:{rpath}"])

def exec_ssh_oneshot(host, cmd):
    return subprocess.run(["ssh", host, cmd])


def exec_ssh(host, cmds):
    ssh_client = paramiko.SSHClient()
    ssh_client.load_system_host_keys()
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_client.connect(hostname=host)
    stdin, stdout, stderr = ssh_client.exec_command("bash -l", get_pty=True)
    cmdstr = cmds + "\nexit\n"
    while not stdout.channel.recv_ready():
        pass
    stdin.write(cmdstr)
    while stdout.channel.recv_exit_status():
        time.sleep(1)
    
    ssh_client.close()

def exec_ssh_env(hostname, cmds):
    project = ep.project()
    host = project.getHostAddress(hostname)
    cmdstr = f"source {project.getEDAFRoot(hostname)}/bin/activate.sh" + "\n" + cmds
    return exec_ssh(host, cmdstr)

def tmux_new_ssh(host, session, cmds=""):
    ssh_client = paramiko.SSHClient()
    ssh_client.load_system_host_keys()
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_client.connect(hostname=host)
    stdin, stdout, stderr = ssh_client.exec_command(f"tmux new-session -s {session} bash", get_pty=True)
    cmdstr = cmds + "\n"
    while not stdout.channel.recv_ready():
        pass
    stdin.write(cmdstr)
    while not stdout.channel.recv_ready():
        pass
    ssh_client.close()

def tmux_new_ssh_env(hostname, session, cmds):
    project = ep.project()
    host = project.getHostAddress(hostname)
    cmdstr = f"source {project.getEDAFRoot(hostname)}/bin/activate.sh" + "\n" + cmds
    return tmux_new_ssh(host, session, cmdstr)

def tmux_attach_ssh(host, session, cmds=""):
    ssh_client = paramiko.SSHClient()
    ssh_client.load_system_host_keys()
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_client.connect(hostname=host)
    stdin, stdout, stderr = ssh_client.exec_command(f"tmux attach-session -t {session}", get_pty=True)
    cmdstr = cmds + "\n"
    while not stdout.channel.recv_ready():
        pass
    stdin.write(cmdstr)
    while not stdout.channel.recv_ready():
        pass
    ssh_client.close()

def tmux_kill_ssh(host, session):
    ssh_client = paramiko.SSHClient()
    ssh_client.load_system_host_keys()
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_client.connect(hostname=host)
    stdin, stdout, stderr = ssh_client.exec_command(f"tmux kill-session -t {session}")
    time.sleep(5)
    ssh_client.close()


def dask_ssh(hostname, nprocs=1):

    ip_external = socket.gethostbyname(ep.project().getHostAddress(hostname))
    # deploy scheduler
    tmux_new_ssh_env(hostname, "dask", f"""
        dask-scheduler --host {ip_external}
    """)

    tmux_new_ssh_env(hostname, "worker", f"""
        dask-worker {ip_external}:8786 \
        --nprocs {nprocs} 
        --nthreads 1\
    """)


def tunnel_ssh(hostname, port):
    ip_external = socket.gethostbyname(ep.project().getHostAddress(hostname))
    host = ep.project().getHostAddress(hostname); # internal ip
    # create tunnel
    tmux_new_ssh_env(hostname, "tunnel", f"ssh -N -L {ip_external}:{port}:{host}:{port} 127.0.0.1")


def dask_pbs(hostname,  njobs=1, nprocs=1):
    
    # create tunnel
    tunnel_ssh(hostname, 8786)

    # deploy scheduler
    tmux_new_ssh_env(hostname, "dask", f"""
        dask-scheduler --host {ep.project().getHostAddress(hostname)}
    """)
    
    # deploy workers
    print(exec_ssh_oneshot(ep.project().getHostAddress(hostname), f""" \
            {ep.project().getEDAFRoot(hostname)}/tools/pbs_dask.sh {ep.project().getHostAddress(hostname)} {nprocs} {njobs}
        """))

