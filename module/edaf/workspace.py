from fs.opener import open_fs
from fs.copy import copy_fs, copy_fs_if_newer
from fs.move import move_fs, move_file
from fs.sshfs import SSHFS
from fs.osfs import OSFS
import shutil
import os
import edaf.util

class workspace:
    def __init__(self, name):
        self.path = None
        self.name = name
        self.content = None
        self.isopen = False

    def create(self, path):
        if self.isopen:
            raise Exception("Workspace already opened")
        self.path = path
        f = open_fs(path)
        f.makedir(self.name)
        self.content = f.opendir(self.name)
        self.isopen = True

    def open(self, path):
        if self.isopen:
            raise Exception("Workspace already opened")        
        self.path = path
        self.content = open_fs(path).opendir(self.name)
        self.isopen = True

    def close(self):
        if not self.isopen:
            raise Exception("Workspace is not opened")        
        self.path = None
        self.content.close()
        self.isopen = False
        
    def copy(self, name, path):
        if not self.isopen:
            raise Exception("Workspace is not opened")
        nfs = open_fs(path)
        nfs.makedir(name, recreate=True)
        nfs = nfs.opendir(name)
        ofs = open_fs(self.path)
        ofs = ofs.opendir(self.name)
        copy_fs_if_newer(ofs, nfs)
        nw = workspace(name)
        nw.open(path)
        return nw

    def move(self, path):
        if not self.isopen:
            raise Exception("Workspace is not opened")
        nfs = open_fs(path)
        ofs = open_fs(self.path)
        ofs = ofs.opendir(self.name)
        shutil.rmtree(f"{path}/{self.name}", ignore_errors=True)
        shutil.move(ofs.getsyspath("."), path)
        nw = workspace(self.name)
        nw.open(path)
        return nw

    def remove(self):
        if not self.isopen:
            raise Exception("Workspace is not opened")
        ofs = open_fs(self.path)
        ofs = ofs.opendir(self.name)
        shutil.rmtree(ofs.getsyspath("."), ignore_errors=True)
        self.close()
    
    def upload(self, rhost, rpath):
        if not self.isopen:
            raise Exception("Workspace is not opened")
        lfs = open_fs(self.path)
        lpath = lfs.opendir(self.name).getospath(".")
        edaf.util.upload_scp(lpath, rhost, rpath)

    def download(self, rhost, rpath, lpath):
        edaf.util.download_scp(rhost, f"{rpath}/{self.name}", lpath)  
        if not self.isopen:
            self.open(lpath)
            self.isopen = True


    
