# Exploratory Data Analysis Framework
# Configuration module

import toml

def read(inp):
    return toml.load(inp)

def write(conf, out):
    return toml.dump(conf, out)