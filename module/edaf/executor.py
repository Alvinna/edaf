from dask.distributed import Client, LocalCluster
import paramiko
import edaf.project as ep
import edaf.util


class executor:
    def __init__(self, hostname, njobs=1, nprocs=1):
        self.project = ep.project()
        self.hostname = hostname
        self.nprocs = nprocs
        self.njobs = njobs
        self.results = None
        self.hosttype = "local"


    def connect(self):
        htype = self.project.getHostType(self.hostname)
        self.hosttype = htype
        if htype == "local":
            cluster = LocalCluster(n_workers=self.njobs)
            self.client = Client(cluster)
            self.client = Client()
        else:
            conn_string = self.project.getHostAddress(self.hostname) + ":8786"
            self.client = Client(conn_string, timeout=100)
        return self.client

    def deploy(self):
        htype = self.project.getHostType(self.hostname)
        self.hosttype = htype
        if htype == "local":
            pass
        elif htype == "ssh":
            edaf.util.dask_ssh(self.hostname, self.njobs)
        elif htype == "pbs":
            edaf.util.dask_pbs(self.hostname, self.njobs, self.nprocs)
        else:
            pass

    def execute(self, function, parameters, names=None):
        self.results = self.client.map(function, parameters, pure=False, key=names)

    def gather(self):
        return self.client.gather(self.results)

    def shutdown(self):
        htype = self.project.getHostType(self.hostname)
        self.hosttype = htype
        self.client.shutdown()
        self.client.close()
        if htype == "local":
            pass
        elif htype == "ssh":
            edaf.util.tmux_kill_ssh(self.project.getHostAddress(self.hostname), "dask")
            edaf.util.tmux_kill_ssh(self.project.getHostAddress(self.hostname), "worker")
        elif htype == "pbs":
            edaf.util.tmux_kill_ssh(self.project.getHostAddress(self.hostname), "dask")
            edaf.util.tmux_kill_ssh(self.project.getHostAddress(self.hostname), "tunnel")
        else:
            pass
