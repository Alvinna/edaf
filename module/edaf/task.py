import papermill as pm
import os
from fs.opener import open_fs
from fs.copy import copy_fs, copy_file, copy_fs_if_newer, copy_file_if_newer
from fs.osfs import OSFS
from fs.path import abspath
from dask.distributed import secede, rejoin
import edaf.project as ep
import edaf.workspace as ew

def executeNotebook(inp, out, param, workingDir):
    for i in range(10):
        try:
            pm.execute_notebook(inp, out, param, cwd=workingDir, report_mode=True)
            break
        except:
            print("Error launching notebook")

def payload_pbs(param):
    name = param["name"]
    template = param["template"]
    shared = param["shared"]
    scratch = param["scratch"]
    noteParam = param["parameters"]

    # copy workspace to scratch dir
    print("Copying workspace to scatch director")
    ws = ew.workspace(name)
    ws.open(shared)
    w = ws.move(scratch)

    # execute notebook
    print("Executing notebook")
    it = f"{scratch}/{name}/{template}.ipynb"
    ot = f"{scratch}/{name}/out_{template}.ipynb"
    os.chdir(f"{scratch}/{name}")
    secede()
    executeNotebook(it, ot, noteParam, f"{scratch}/{name}")
    rejoin()
    # copy workspace back to shared dir
    print("Copying workspace to shared director")
    w.move(shared)

def payload_ssh(param):
    name = param["name"]
    template = param["template"]
    scratch = param["scratch"]
    noteParam = param["parameters"]

    w = ew.workspace(name)
    w.open(scratch)

    # execute notebook
    print("Executing notebook")
    it = f"{scratch}/{name}/{template}.ipynb"
    ot = f"{scratch}/{name}/out_{template}.ipynb"
    os.chdir(f"{scratch}/{name}")
    secede()
    executeNotebook(it, ot, noteParam, f"{scratch}/{name}")
    rejoin()

def payload_local(param):
    name = param["name"]
    template = param["template"]
    scratch = param["scratch"]
    noteParam = param["parameters"]

    w = ew.workspace(name)
    w.open(scratch)

    # execute notebook
    print("Executing notebook")
    it = f"{scratch}/{name}/{template}.ipynb"
    ot = f"{scratch}/{name}/out_{template}.ipynb"
    os.chdir(f"{scratch}/{name}")
    secede()
    executeNotebook(it, ot, noteParam, f"{scratch}/{name}")
    rejoin()

class task:
    def __init__(self, executor, template, workspace):
        
        # if single workspace
        if not isinstance(workspace, list):
            self.executor = executor
            self.inputWorkspace = workspace
            self.workspace = None
            self.template = template
            self.name = self.inputWorkspace.name
            self.ismult = False
        else:
            self.executor = executor
            self.inputWorkspace = workspace
            self.workspace = None
            self.template = template
            self.name = [w.name for w in self.inputWorkspace]
            self.ismult = True

    def run(self, parameters=None):

        if self.ismult == False:
            if not parameters:
                parameters = dict()
            # move workspace to tmp
            tmpdir = ep.project().hosts.get("localhost").get("scratch")
            print(tmpdir)
            wpath = self.inputWorkspace.path
            self.workspace = self.inputWorkspace.move(tmpdir)

            # move template to tmp workspace
            templatedir = f"{ep.project().root}/template"
            fi = open_fs(templatedir)
            print(templatedir)
            fo = self.workspace.content
            copy_file_if_newer(fi, f"{str(self.template)}.ipynb", fo, f"{str(self.template)}.ipynb")

            # upload workspace 
            htype = self.executor.hosttype
            print(htype)
            pm = parameters
            pm["nprocs"] = self.executor.nprocs
            if htype == "local":
                scratch = ep.project().hosts.get("localhost").get("scratch")
                self.executor.execute(payload_local, [dict(
                    name=self.name,
                    template=self.template,
                    scratch=scratch,
                    parameters=pm
                )])
                self.executor.gather()
                self.workspace = self.workspace.move(wpath)

            elif htype == "ssh":
                scratch = ep.project().hosts.get(self.executor.hostname).get("scratch")
                self.workspace.upload(ep.project().getHostAddress(self.executor.hostname), scratch)
                self.executor.execute(payload_ssh, [dict(
                    name=self.name,
                    template=self.template,
                    scratch=scratch,
                    parameters=pm
                )])
                self.executor.gather()
                self.workspace.close()
                self.workspace.download(ep.project().getHostAddress(self.executor.hostname), scratch, wpath)

            elif htype == "pbs":
                shared = ep.project().hosts.get(self.executor.hostname).get("shared")
                scratch = ep.project().hosts.get(self.executor.hostname).get("scratch")
                self.workspace.upload(ep.project().getHostAddress(self.executor.hostname), shared)
                self.executor.execute(payload_pbs, [dict(
                    name=self.name,
                    template=self.template,
                    shared=shared,
                    scratch=scratch,
                    parameters=pm
                )])
                self.executor.gather()
                self.workspace.close()
                self.workspace.download(ep.project().getHostAddress(self.executor.hostname), shared, wpath)
            else:
                pass
        # if multiple workspaces are passed
        else:
            if not parameters:
                parameters = [{}] * self.executor.njobs
    # move workspace to tmp
            tmpdir = ep.project().hosts.get("localhost").get("scratch")
            print(tmpdir)
            wpath = [w.path for w in self.inputWorkspace]
            self.workspace = [w.move(tmpdir) for w in self.inputWorkspace]

            # move template to tmp workspace
            templatedir = f"{ep.project().root}/template"
            fi = open_fs(templatedir)
            print(templatedir)
            for w in self.workspace:
                fo = w.content
                copy_file_if_newer(fi, f"{str(self.template)}.ipynb", fo, f"{str(self.template)}.ipynb")

            # upload workspace 
            htype = self.executor.hosttype
            print(htype)
            pm = []
            for d in parameters:
                d["nprocs"] = self.executor.nprocs
                pm.append(d)
            print(pm)
            if htype == "local":
                scratch = ep.project().hosts.get("localhost").get("scratch")
                self.executor.execute(payload_local, [dict(
                    name=n,
                    template=self.template,
                    scratch=scratch,
                    parameters=p
                ) for n, p in zip(self.name, pm)])
                self.executor.gather()

                self.workspace = [w.move(wp) for w, wp in zip(self.workspace, wpath)]

            elif htype == "ssh":
                scratch = ep.project().hosts.get(self.executor.hostname).get("scratch")
                for w in self.workspace:
                    w.upload(ep.project().getHostAddress(self.executor.hostname), scratch)

                self.executor.execute(payload_ssh, [dict(
                    name=n,
                    template=self.template,
                    scratch=scratch,
                    parameters=p
                ) for n, p in zip(self.name, pm)])
                self.executor.gather()
                for w, wp in zip(self.workspace, wpath):
                    w.close()
                    w.download(ep.project().getHostAddress(self.executor.hostname), scratch, wp)

            elif htype == "pbs":
                shared = ep.project().hosts.get(self.executor.hostname).get("shared")
                scratch = ep.project().hosts.get(self.executor.hostname).get("scratch")
                for w in self.workspace:
                    w.upload(ep.project().getHostAddress(self.executor.hostname), shared)

                self.executor.execute(payload_pbs, [dict(
                    name=n,
                    template=self.template,
                    shared=shared,
                    scratch=scratch,
                    parameters=p
                ) for n, p in zip(self.name, pm)])
                self.executor.gather()
                for w, wp in zip(self.workspace, wpath):
                    w.close()
                    w.download(ep.project().getHostAddress(self.executor.hostname), shared, wp)
            else:
                pass

        





    

